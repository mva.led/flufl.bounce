======================================
flufl.bounce - Email bounce detectors.
======================================

The `flufl.bounce` library provides a set of heuristics and an API for
detecting the original bouncing email addresses from a bounce message.  Many
formats found in the wild are supported, as are VERP_ and RFC 3464 (DSN_).


Requirements
============

`flufl.bounce` requires Python 2.6 or newer, and is compatible with
Python 3.  The `zope.interface`_ package is a requirement.


Documentation
=============

A `simple guide`_ to using the library is available within this package, in
the form of doctests.


Project details
===============

 * Project home: https://gitlab.com/warsaw/flufl.bounce
 * Report bugs at: https://gitlab.com/warsaw/flufl.bounce/issues
 * Code hosting: git@gitlab.com:warsaw/flufl.bounce.git
 * Documentation: http://fluflbounce.readthedocs.org/

you can install it with ``pip``::

    % pip install flufl.bounce

You can grab the latest development copy of the code using git.  The master
repository is hosted on GitLab.  If you have git installed, you can grab
your own branch of the code like this::

    $ git clone git@gitlab.com:warsaw/flufl.bounce.git

You may contact the author via barry@python.org.


Copyright
=========

Copyright (C) 2004-2016 Barry A. Warsaw

This file is part of flufl.bounce.

flufl.bounce is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

flufl.bounce is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
details.

You should have received a copy of the GNU Lesser General Public License along
with flufl.bounce.  If not, see <http://www.gnu.org/licenses/>.


Table of Contents
=================

.. toctree::

    docs/using.rst
    NEWS.rst

.. _DSN: http://www.faqs.org/rfcs/rfc3464.html
.. _VERP: http://en.wikipedia.org/wiki/Variable_envelope_return_path
.. _`simple guide`: docs/using.html
.. _`virtualenv`: http://www.virtualenv.org/en/latest/index.html
.. _`zope.interface`: https://pypi.python.org/pypi/zope.interface
